package com.bunhann.advancerecycleview;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.bunhann.advancerecycleview.utils.LocaleHelper;

import cyd.awesome.material.AwesomeButton;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private AwesomeButton btn1, btn2, btn3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        btn1 = (AwesomeButton) findViewById(R.id.btnRecycleView1);
        btn2 = (AwesomeButton) findViewById(R.id.btnRecycleView2);
        btn3 = (AwesomeButton) findViewById(R.id.btnRecycleView3);
        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.menu_action_usa:
                //Change Application level locale
                LocaleHelper.setLocale(MainActivity.this, "en");
                //It is required to recreate the activity to reflect the change in UI.
                recreate();
                return true;
            case R.id.menu_action_km:
                //Change Application level locale
                LocaleHelper.setLocale(MainActivity.this, "km");
                //It is required to recreate the activity to reflect the change in UI.
                recreate();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        Intent intent = null;
        switch (id){
            case R.id.btnRecycleView1:
                intent = new Intent(v.getContext(), RecyclerWithCardActivity.class);
                break;
            case R.id.btnRecycleView2:
                intent = new Intent(v.getContext(), RecyclerWithSwipe.class);
                break;
            case R.id.btnRecycleView3:
                intent = new Intent(v.getContext(), RecyclerWithFilterActivity.class);
                break;
        }
        startActivity(intent);
    }
}
