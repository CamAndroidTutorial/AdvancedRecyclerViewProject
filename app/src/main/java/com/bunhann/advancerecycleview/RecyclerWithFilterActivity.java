package com.bunhann.advancerecycleview;

import android.app.SearchManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.bunhann.advancerecycleview.adapter.AlbumAdapterFilterable;
import com.bunhann.advancerecycleview.adapter.AlbumAdapterListener;
import com.bunhann.advancerecycleview.models.Album;
import com.bunhann.advancerecycleview.utils.MyDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

public class RecyclerWithFilterActivity extends AppCompatActivity implements AlbumAdapterListener{

    private static final String TAG = RecyclerWithFilterActivity.class.getSimpleName();
    private RecyclerView recyclerView;
    private List<Album> albumList;
    private AlbumAdapterFilterable adapterFilterable;
    private SearchView searchView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_filter_layout);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.btn3);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_filter);
        albumList = new ArrayList<>();

        adapterFilterable = new AlbumAdapterFilterable(this, albumList, this);

        // white background notification bar
        whiteNotificationBar(recyclerView);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new MyDividerItemDecoration(this, DividerItemDecoration.VERTICAL, 36));
        recyclerView.setAdapter(adapterFilterable);

        prepareAlbums();
    }

    @Override
    public void onContactSelected(Album album) {
        Toast.makeText(this, "You clicked on: " + album.getName(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        // close search view on back button pressed
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            return;
        }
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_filter, menu);
        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        // listening to search query text change
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                adapterFilterable.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when text is changed
                adapterFilterable.getFilter().filter(query);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            case R.id.action_search:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void whiteNotificationBar(View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int flags = view.getSystemUiVisibility();
            flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
            view.setSystemUiVisibility(flags);
            getWindow().setStatusBarColor(Color.WHITE);
        }
    }

    /**
     * Adding few albums for testing
     */
    private void prepareAlbums() {
        int[] covers = new int[]{
                R.drawable.img1,
                R.drawable.img2,
                R.drawable.img3,
                R.drawable.img4,
                R.drawable.img5,
                R.drawable.img6,
                R.drawable.bg,
                R.drawable.ic_search};

        Album a = new Album("អប្សរា ទី១", 1, covers[0]);
        albumList.add(a);

        a = new Album("អប្សរា ទី២", 8, covers[1]);
        albumList.add(a);

        a = new Album("អប្សរា ទី៣", 4, covers[2]);
        albumList.add(a);

        a = new Album("អប្សរា ទី៤", 3, covers[3]);
        albumList.add(a);

        a = new Album("អប្សរា ទី៥", 2, covers[4]);
        albumList.add(a);

        a = new Album("អប្សរា ទី៦", 1, covers[5]);
        albumList.add(a);

        a = new Album("ABC", 1, covers[6]);
        albumList.add(a);

        a = new Album("John", 10, covers[1]);
        albumList.add(a);

        a = new Album("Hennry", 1, covers[7]);
        albumList.add(a);

        a = new Album("កម្ពុជា", 1, covers[2]);
        albumList.add(a);

        a = new Album("អាមេរិច", 10, covers[7]);
        albumList.add(a);

        a = new Album("ហុងកុង", 8, covers[5]);
        albumList.add(a);

        a = new Album("ចិន", 18, covers[6]);
        albumList.add(a);

        adapterFilterable.notifyDataSetChanged();
    }
}
