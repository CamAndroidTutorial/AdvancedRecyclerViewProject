package com.bunhann.advancerecycleview;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.MenuItem;
import android.view.View;

import com.bunhann.advancerecycleview.adapter.AlbumAdapterSwipe;
import com.bunhann.advancerecycleview.models.Album;
import com.bunhann.advancerecycleview.utils.RecyclerItemTouchHelper;

import java.util.ArrayList;
import java.util.List;

public class RecyclerWithSwipe extends AppCompatActivity implements RecyclerItemTouchHelper.RecyclerItemTouchHelperListener{

    private RecyclerView recyclerView;
    private List<Album> albumList;
    private AlbumAdapterSwipe albumAdapterSwipe;
    private CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_filter_layout);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.btn2);

        recyclerView = findViewById(R.id.recycler_view_filter);
        coordinatorLayout = findViewById(R.id.coordinator_layout);
        albumList = new ArrayList<>();
        albumAdapterSwipe = new AlbumAdapterSwipe(this, albumList);


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(albumAdapterSwipe);

        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);

        prepareAlbums();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof AlbumAdapterSwipe.MyViewHolder) {
            // get the removed item name to display it in snack bar
            String name = albumList.get(viewHolder.getAdapterPosition()).getName();
            // backup of removed item for undo purpose
            final Album deletedAlbum = albumList.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();
            // remove the item from recycler view
            albumAdapterSwipe.removeItem(viewHolder.getAdapterPosition());

            // showing snack bar with Undo option
            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, name + " removed from list!", Snackbar.LENGTH_LONG);
            snackbar.setAction("UNDO", new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // undo is selected, restore the deleted item
                    albumAdapterSwipe.restoreItem(deletedAlbum, deletedIndex);
                }
            });
            snackbar.setActionTextColor(Color.YELLOW);
            snackbar.show();
        }
    }

    /**
     * Adding few albums for testing
     */
    private void prepareAlbums() {
        int[] covers = new int[]{
                R.drawable.img1,
                R.drawable.img2,
                R.drawable.img3,
                R.drawable.img4,
                R.drawable.img5,
                R.drawable.img6,
                R.drawable.bg,
                R.drawable.ic_search};

        Album a = new Album("អប្សរា ទី១", 1, covers[0]);
        albumList.add(a);

        a = new Album("អប្សរា ទី២", 8, covers[1]);
        albumList.add(a);

        a = new Album("អប្សរា ទី៣", 4, covers[2]);
        albumList.add(a);

        a = new Album("អប្សរា ទី៤", 3, covers[3]);
        albumList.add(a);

        a = new Album("អប្សរា ទី៥", 2, covers[4]);
        albumList.add(a);

        a = new Album("អប្សរា ទី៦", 1, covers[5]);
        albumList.add(a);

        a = new Album("ABC", 1, covers[6]);
        albumList.add(a);

        a = new Album("John", 10, covers[1]);
        albumList.add(a);

        a = new Album("Hennry", 1, covers[7]);
        albumList.add(a);

        a = new Album("កម្ពុជា", 1, covers[2]);
        albumList.add(a);

        a = new Album("អាមេរិច", 10, covers[7]);
        albumList.add(a);

        a = new Album("ហុងកុង", 8, covers[5]);
        albumList.add(a);

        a = new Album("ចិន", 18, covers[6]);
        albumList.add(a);

        albumAdapterSwipe.notifyDataSetChanged();
    }
}
