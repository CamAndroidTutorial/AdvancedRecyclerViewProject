package com.bunhann.advancerecycleview.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bunhann.advancerecycleview.R;
import com.bunhann.advancerecycleview.models.Album;

import java.util.List;

public class AlbumAdapterSwipe extends RecyclerView.Adapter<AlbumAdapterSwipe.MyViewHolder>{

    private Context context;
    private List<Album> albumList;
    private LayoutInflater inflater;

    public AlbumAdapterSwipe(Context context, List<Album> albumList) {
        this.context = context;
        this.albumList = albumList;
        this.inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View convertView = inflater.inflate(R.layout.album_list_item, parent, false);

        return new MyViewHolder(convertView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final Album album = albumList.get(position);
        holder.name.setText(album.getName());
        holder.numSong.setText("Number: " + String.valueOf(album.getNumOfSongs()));
        Glide.with(context).load(album.getThumbnail())
                .apply(RequestOptions.circleCropTransform())
                .into(holder.thumbnail);
    }

    @Override
    public int getItemCount() {
        return albumList.size();
    }


    public void removeItem(int position) {
        albumList.remove(position);

        notifyItemRemoved(position);
    }

    public void restoreItem(Album album, int position) {
        albumList.add(position, album);
        notifyItemInserted(position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, numSong;
        public ImageView thumbnail;

        public RelativeLayout viewBackground, viewForeground;
        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.tvTitle);
            numSong = view.findViewById(R.id.tvNumberSong);
            thumbnail = view.findViewById(R.id.imgAlbum);
            viewBackground = view.findViewById(R.id.view_background);
            viewForeground = view.findViewById(R.id.view_foreground);
        }
    }
}
