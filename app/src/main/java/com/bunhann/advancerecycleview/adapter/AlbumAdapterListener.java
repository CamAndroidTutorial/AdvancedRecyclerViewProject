package com.bunhann.advancerecycleview.adapter;

import com.bunhann.advancerecycleview.models.Album;

public interface AlbumAdapterListener {

    void onContactSelected(Album album);

}
