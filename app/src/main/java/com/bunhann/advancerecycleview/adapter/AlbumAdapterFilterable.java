package com.bunhann.advancerecycleview.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bunhann.advancerecycleview.R;
import com.bunhann.advancerecycleview.models.Album;

import java.util.ArrayList;
import java.util.List;

public class AlbumAdapterFilterable extends RecyclerView.Adapter<AlbumAdapterFilterable.MyViewHolder> implements Filterable{


    private Context context;
    private List<Album> albumList;
    private List<Album> albumListFiltered;
    private AlbumAdapterListener listener;
    private LayoutInflater inflater;

    public AlbumAdapterFilterable(Context context, List<Album> albumList, AlbumAdapterListener listener) {
        this.context = context;
        this.albumList = albumList;
        this.listener = listener;
        this.albumListFiltered = albumList;
        this.inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View convertView = inflater.inflate(R.layout.album_row_item, parent, false);

        return new MyViewHolder(convertView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final Album album = albumListFiltered.get(position);
        holder.name.setText(album.getName());
        holder.numSong.setText("Number: " + String.valueOf(album.getNumOfSongs()));
        Glide.with(context).load(album.getThumbnail())
                .apply(RequestOptions.circleCropTransform())
                .into(holder.thumbnail);

    }

    @Override
    public int getItemCount() {
        return albumListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charString = constraint.toString();
                if (charString.isEmpty()){
                    albumListFiltered = albumList;
                } else {
                    List<Album> filteredList = new ArrayList<>();
                    for (Album row: albumList){
                        if (row.getName().toLowerCase().contains(charString.toLowerCase())){
                            filteredList.add(row);
                        }
                    }
                    albumListFiltered = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = albumListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                albumListFiltered = (ArrayList<Album>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, numSong;
        public ImageView thumbnail;

        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.tvTitle);
            numSong = view.findViewById(R.id.tvNumberSong);
            thumbnail = view.findViewById(R.id.imgAlbum);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // send selected contact in callback
                    listener.onContactSelected(albumListFiltered.get(getAdapterPosition()));
                }
            });
        }
    }
}
